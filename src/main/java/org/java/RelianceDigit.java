package org.java;

import java.awt.AWTException;

import javax.swing.Action;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.reporters.jq.Main;

import io.github.bonigarcia.wdm.WebDriverManager;

public class RelianceDigit {
	public static void main(String[] args) throws InterruptedException, AWTException {
		
	WebDriverManager.chromedriver().setup();
	WebDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.reliancedigital.in/");
	Thread.sleep(3000);
	driver.findElement(By.xpath("//button[text()='No, don�t.']")).click();
	//
	//
	WebElement television = driver.findElement(By.xpath("//div[text()='Televisions']"));
	television.click();
	Actions action=new Actions(driver);
	action.moveToElement(television).perform();
	driver.findElement(By.xpath("//a[text()='32 Inch TVs']")).click();
	Thread.sleep(4000);
	//32 inch tv
	for (int i = 1; i <=24; i++) {
		WebElement tv = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[2]/div[1]/div[3]/ul/li["+i+"]/div/a/div/div[2]"));
		String text = tv.getText();
		System.out.println(text);
		
	}
	Thread.sleep(5000);
	driver.navigate().back();
	television.click();
	Actions action1=new Actions(driver);
	action1.moveToElement(television).perform();
	driver.findElement(By.xpath("//a[text()='43 Inch TVs']")).click();
	Thread.sleep(4000);
	//43inch tv
	for (int i = 1; i <=24; i++) {
		WebElement tv = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[2]/div[1]/div[3]/ul/li["+i+"]/div/a/div/div[2]"));
		String text = tv.getText();
		System.out.println(text);
	
	}
	Thread.sleep(3000);
	driver.navigate().back();
	television.click();
	Actions action2=new Actions(driver);
	action2.moveToElement(television).perform();
	driver.findElement(By.xpath("//a[text()='55 Inch TVs']")).click();
	Thread.sleep(4000);
	//55 inch tv
	for (int i = 1; i <=24; i++) {
		WebElement tv = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[2]/div[1]/div[3]/ul/li["+i+"]/div/a/div/div[2]"));
		String text = tv.getText();
		System.out.println(text);
	
	}
	Thread.sleep(3000);
	WebElement mobile = driver.findElement(By.xpath("//div[text()='Mobiles & Tablets']"));
	mobile.click();
	Actions action3=new Actions(driver);
	action3.moveToElement(mobile).perform();
	driver.findElement(By.xpath("//a[text()='New Launches']")).click();
	Thread.sleep(4000);
	//watch
	for (int i = 1; i <=24; i++) {
		WebElement watch = driver.findElement(By.xpath("//*[@id=\"pl\"]/div[3]/ul/li["+i+"]/div/a/div/div[2]"));
		String watchdetails = watch.getText();
		System.out.println(watchdetails);
		
	}
	}
	
}
