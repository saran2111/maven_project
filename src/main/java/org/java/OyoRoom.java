package org.java;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class OyoRoom {
	public static void main(String[] args) throws InterruptedException, AWTException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		driver.manage().window().maximize();
		driver.get("https://www.oyorooms.com/");
		Robot robot=new Robot();
		// location
		WebElement 
		findLocation = driver.findElement(By.id("autoComplete__home"));
		findLocation.sendKeys("chennai");
		Thread.sleep(2000);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		
		// Thread.sleep(2000);
		WebElement findArea = driver.findElement(By.xpath("(//div[contains(@class,'oyo-cell ')])[6]"));
		wait.until(ExpectedConditions.visibilityOf(findArea));
		findArea.click();
		// date
		WebElement date = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div[3]/div/div/div/div[2]/div/div"));
		date.click();
		WebElement clickDate1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div[3]/div/div[1]/div/div[2]/div/span/div/div/div[2]/table/tbody/tr[3]/td[6]"));
		clickDate1.click();
		WebElement clickDate2 = driver
				.findElement(By.xpath("(//td[contains(@class,'DateRangePicker__Date')])[22]"));
		clickDate2.click();
		// Room
		WebElement clickRoom = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div[3]/div/div/div/div[3]/div/div"));
		clickRoom.click();
		WebElement clickPlus = driver.findElement(By.className("guestRoomPickerPopUp__plus"));
		// wait.until(ExpectedConditions.visibilityOf(clickPlus));
		clickPlus.click();
		clickRoom.click();
		// search
		WebElement search = driver.findElement(By.xpath("//button[contains(@class,'u-textCenter')]"));
		search.click();
	  Thread.sleep(2000);
		for (int i = 1; i <=41; i++) {
			
			WebElement data = driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div/div[2]/section/div/div[2]/div["+1+"]/div/div[2]/div/div[1]"));
			String text = data.getText();
			System.out.println(text);
		}
		

	}
}
