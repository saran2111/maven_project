package org.java;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ScrollDown {
	public static void main(String[] args) throws IOException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.amazon.in");
		WebElement searchBox = driver.findElement(By.name("field-keywords"));
		searchBox.sendKeys("Iphone" + Keys.ENTER);
		WebElement clickIphone = driver.findElement(By.xpath("//h2[contains(@class,'a-size-mini')]"));
		clickIphone.click();
		// Move to parent
		String parentWind = driver.getWindowHandle();
		System.out.println(parentWind);
		driver.switchTo().window(parentWind);
		// down
		WebElement down = driver.findElement(By.xpath("//div[contains(@class,'nav-logo')]"));
		JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].scrollIntoView(true)", down);

		// Screenshot
		TakesScreenshot s = (TakesScreenshot) driver;
		File screenShot = s.getScreenshotAs(OutputType.FILE);
		File a = new File("C:\\Users\\SARANYA\\eclipse-workspace\\MavenPjt\\Screenshot\\b1.png");
		FileUtils.copyFile(screenShot, a);
		// child
		Set<String> allWin = driver.getWindowHandles();
		for (String childWin : allWin) {
			if (!parentWind.equals(childWin)) {
				System.out.println(childWin);
				driver.switchTo().window(childWin);
				WebElement text = driver.findElement(By.xpath("//span[@id='productTitle']"));
				String b = text.getText();
				System.out.println(b);
				// scrollDown
				WebElement scrollDown = driver.findElement(By.xpath("//a[text()='Italy']"));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].scrollIntoView(true)", scrollDown);

			}
		}
	}
}
