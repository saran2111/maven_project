package org.java;

import java.awt.AWTException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class relinc {
	public static void main(String[] args) throws InterruptedException, AWTException {

		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.reliancedigital.in/");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='No, don�t.']")).click();
		//television
		for (int i = 3; i <= 5; i++) {
			WebElement television = driver.findElement(By.xpath("//div[text()='Televisions']"));
			television.click();
			Actions action = new Actions(driver);
			action.moveToElement(television).perform();
			driver.findElement(
					By.xpath("/html/body/div[1]/header/div/nav/ul/li[2]/div[2]/div/div[1]/ul/li[" + i + "]/a")).click();
			Thread.sleep(5000);
			// tv
			for (int j = 1; j <= 24; j++) {
				WebElement tv = driver.findElement(By.xpath(
						"/html/body/div[1]/div[2]/div[2]/div[2]/div[1]/div[3]/ul/li[" + j + "]/div/a/div/div[2]"));
				String text = tv.getText();
				System.out.println(text);

			}
			Thread.sleep(2000);
			driver.navigate().back();

		}

		WebElement mobile = driver.findElement(By.xpath("//div[text()='Mobiles & Tablets']"));
		mobile.click();
		Actions action3 = new Actions(driver);
		action3.moveToElement(mobile).perform();
		driver.findElement(By.xpath("//a[text()='New Launches']")).click();
		Thread.sleep(4000);
		// watch
		for (int i = 1; i <= 24; i++) {
			WebElement watch = driver.findElement(By.xpath("//*[@id=\"pl\"]/div[3]/ul/li[" + i + "]/div/a/div/div[2]"));
			String watchdetails = watch.getText();
			System.out.println(watchdetails);

		}
		driver.quit();
	}

}
