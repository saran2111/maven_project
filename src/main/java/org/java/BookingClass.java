package org.java;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BookingClass {
	public static void main(String[] args)throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.booking.com/");
		Thread.sleep(3000);
		WebElement click = driver.findElement(By.xpath("(//button[contains(@class,'fc63351294 ')])[26]"));
		click.click();
		WebElement carRental = driver.findElement(By.xpath("(//span[@class='db29ecfbe2'])[4]"));
		carRental.click();
		WebElement pickUp = driver.findElement(By.xpath("//label[contains(@class,'LPCM_68872a86')] "));
		pickUp.click();

		WebElement location = driver.findElement(By.xpath(
				"/html/body/div[2]/div[2]/div/div/div/div/div/div[2]/div/div[1]/div/div[1]/span/div/div/div/div[2]/div/input"));
		location.sendKeys("chennai");
		Thread.sleep(1000);
		WebElement click2 = driver.findElement(By.xpath("//div[contains(@class,'LPCM_ff31c0fa ')]"));
		click2.click();
		WebElement dateclk = driver.findElement(By.xpath("(//button[@class='sbc-fl-button'])[1]"));
		dateclk.click();
		WebElement date1 = driver.findElement(By.xpath("//span[@data-date='2023-05-07']"));
		date1.click();
		WebElement date2 = driver.findElement(By.xpath("//span[@data-date='2023-05-11']"));
		date2.click();
		// select
		WebElement pickTime = driver.findElement(By.xpath("//select[@name='pickup-time']"));
		Select time = new Select(pickTime);
		time.selectByIndex(20);
		WebElement dropTime = driver.findElement(By.xpath(
				"/html/body/div[2]/div[2]/div/div/div/div/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div/div/div/select"));
		Select time2 = new Select(dropTime);
		time2.selectByIndex(40);
		WebElement search = driver.findElement(By.cssSelector("div.LPCM_f3f1fc59"));
		search.click();

	}



		
		
	}


