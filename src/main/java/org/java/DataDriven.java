package org.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DataDriven {
	public static void main(String[] args) throws FileNotFoundException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		File f = new File("C:\\Users\\SARANYA\\eclipse-workspace\\MavenPjt\\example.xlsx");
		FileInputStream fi = new FileInputStream(f);
		Workbook wb = null;
		try {
			wb = new XSSFWorkbook(fi);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sheet sheet = wb.getSheet("sheet1");
		Row row = sheet.getRow(0);
		Cell cell = row.getCell(0);
		String stringCellValue = cell.getStringCellValue();
		// System.out.println(stringCellValue);
		driver.get(stringCellValue);
		Cell cell2 = row.getCell(1);
		String stringCellValue2 = cell2.getStringCellValue();
		System.out.println(stringCellValue2);

		Row row2 = sheet.getRow(1);
		Cell cell3 = row2.getCell(0);
		String stringCellValue3 = cell3.getStringCellValue();
		driver.get(stringCellValue3);
		Cell cell4 = row2.getCell(1);
		String stringCellValue4 = cell4.getStringCellValue();
		System.out.println(stringCellValue4);

	}

}
