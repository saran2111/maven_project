package org.java;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EmailClass {
	public static void main(String[] args) throws IOException, InterruptedException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		FileReader f = new FileReader("C:\\Users\\SARANYA\\eclipse-workspace\\MavenPjt\\input\\file.properties");
		Properties p = new Properties();
		p.load(f);
		driver.get(p.getProperty("url"));
		Thread.sleep(1000);
		WebElement userClick = driver.findElement(By.xpath("(//input[@class='_aa4b _add6 _ac4d'])[1]"));
		userClick.click();
		Thread.sleep(2000);
		WebElement userText = driver.findElement(By.xpath("//input[contains(@aria-label,'username')]"));
		userText.sendKeys(p.getProperty("user"));
		Thread.sleep(1000);

		WebElement passClick = driver.findElement(By.xpath("//input[contains(@aria-label,'Password')]"));
		passClick.click();
		Thread.sleep(2000);
		WebElement passText = driver.findElement(By.xpath("(//input[@aria-required='true'])[2]"));
		passText.sendKeys(p.getProperty("password") + Keys.ENTER);

	}

}
