package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestNgClass {
	public static WebDriver driver;

	@BeforeTest
	private void login() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://opensource-demo.orangehrmlive.com/");

		// login
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		WebElement username = driver.findElement(By.xpath("//input[@name='username']"));
		username.click();
		username.sendKeys("Admin");
		// password
		WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
		password.click();
		password.sendKeys("admin123");
		// submit
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	private void admin() {
		driver.findElement(By.xpath("//a[@class='oxd-main-menu-item']")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.xpath("(//span[@class='oxd-topbar-body-nav-tab-item'])[2]")).click();
		driver.findElement(By.xpath("//a[text()='Job Titles']")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.xpath("//i[contains(@class,'oxd-icon bi-plus')]")).click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement title = driver.findElement(By.xpath("(//input[@class='oxd-input oxd-input--active'])[2]"));
		title.click();
		title.sendKeys("Software Tester");
		// job description
		WebElement jobDesc = driver.findElement(By.xpath("//textarea[@placeholder='Type description here']"));
		jobDesc.click();
		jobDesc.sendKeys("Design & Execution of test scripts");
		// add note
		WebElement note = driver.findElement(By.xpath("//textarea[@placeholder='Add note']"));
		note.click();
		note.sendKeys("0-3 Yrs Experience");
		// submit
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.xpath("//div[text()='Software Tester']/../preceding-sibling::div")).click();
		driver.findElement(
				By.xpath("//span[text()='Design & Execution of test scripts']/../../following-sibling::div//button"))
				.click();
		driver.findElement(By.xpath("//i[contains(@class,'oxd-icon bi-trash ')]")).click();
	}
    @AfterTest
	private void logout() {
    	driver.findElement(By.className("oxd-userdropdown-tab")).click();
    	driver.findElement(By.xpath("//a[text()='Logout']")).click();
    	driver.close();
    	driver.quit();
    	

	}
}
